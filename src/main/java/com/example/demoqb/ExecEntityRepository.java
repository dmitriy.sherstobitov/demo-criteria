package com.example.demoqb;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.ZonedDateTime;

public interface ExecEntityRepository extends JpaRepository<ExecEntity, Long> {

    @Query(value = "SELECT count(*) from ExecEntity " +
                   "WHERE level = :level " +
                   "AND modelId = :modelId " +
                   "AND timeCalculated >= :epochTimeMin " +
                   "AND timeCalculated <= :epochTimeMax")
    Long getCount(@Param("level") long level,
                  @Param("modelId") long modelId,
                  @Param("epochTimeMin") ZonedDateTime epochTimeMin,
                  @Param("epochTimeMax") ZonedDateTime epochTimeMax);
}
