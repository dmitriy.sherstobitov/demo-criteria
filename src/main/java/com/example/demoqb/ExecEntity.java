package com.example.demoqb;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.ZonedDateTime;

@Entity
public class ExecEntity {

    @Id
    long id;
    long level;
    long modelId;
    ZonedDateTime epochTimeMax;
    ZonedDateTime epochTimeMin;
    ZonedDateTime timeCalculated;

    public ExecEntity(long id,
                      long level,
                      long modelId,
                      ZonedDateTime epochTimeMax,
                      ZonedDateTime epochTimeMin,
                      ZonedDateTime timeCalculated) {
        this.id = id;
        this.level = level;
        this.modelId = modelId;
        this.epochTimeMax = epochTimeMax;
        this.epochTimeMin = epochTimeMin;
        this.timeCalculated = timeCalculated;
    }

    public ExecEntity() {
    }

    @Override
    public String toString() {
        return "ExecEntity{" +
               "id=" + id +
               ", level=" + level +
               ", modelId=" + modelId +
               ", epochTimeMax=" + epochTimeMax +
               ", epochTimeMin=" + epochTimeMin +
               ", timeCalculated=" + timeCalculated +
               '}';
    }
}
