package com.example.demoqb;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.time.ZoneId;
import java.time.ZonedDateTime;

@SpringBootApplication
public class DemoqbApplication implements CommandLineRunner {

    private final EntityManagerFactory entityManagerFactory;
    private final ExecEntityRepository execEntityRepository;
    private long LEVEL = 5;
    private long MODEL_ID = 5;
    private ZonedDateTime EPOCH_TIME_MIN = ZonedDateTime.of(2,2,2,2,2,2,2, ZoneId.systemDefault());
    private ZonedDateTime EPOCH_TIME_MAX = ZonedDateTime.of(7,7,7,7,7,7,7, ZoneId.systemDefault());

    public DemoqbApplication(EntityManagerFactory entityManagerFactory,
                             ExecEntityRepository execEntityRepository) {
        this.entityManagerFactory = entityManagerFactory;
        this.execEntityRepository = execEntityRepository;
    }

    public static void main(String[] args) {
        SpringApplication.run(DemoqbApplication.class, args);
    }

    @Override
    public void run(String... args) {
        final SessionFactory sessionFactory = entityManagerFactory.unwrap(SessionFactory.class);
        final Session session = sessionFactory.openSession();

        buildExampleElements();

        execEntityRepository.findAll().forEach(System.out::println);

        final Long result = executeCriteriaQuery(session, LEVEL, MODEL_ID, EPOCH_TIME_MIN, EPOCH_TIME_MAX);
        System.out.println(result);

        final Long result2 = executeCriteriaQuery(session, MODEL_ID, EPOCH_TIME_MIN, EPOCH_TIME_MAX);
        System.out.println(result2);

        final Long repositoryResult = execEntityRepository.getCount(LEVEL, MODEL_ID, EPOCH_TIME_MIN, EPOCH_TIME_MAX);
        System.out.println(repositoryResult);
    }

    private void buildExampleElements() {
        for (int i = 1; i < 10; i++) {
            final ZonedDateTime timeCalculated = ZonedDateTime.of(i, i, i, i, i, i, i, ZoneId.systemDefault());
            execEntityRepository.save(new ExecEntity(i, LEVEL, MODEL_ID, timeCalculated, timeCalculated, timeCalculated));
        }
    }

    private Long executeCriteriaQuery(Session session,
                                      long level,
                                      long modelId,
                                      ZonedDateTime epochTimeMin,
                                      ZonedDateTime epochTimeMax) {
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<ExecEntity> root = criteriaQuery.from(ExecEntity.class);

        criteriaQuery.select(criteriaBuilder.count(root))
                .where(criteriaBuilder.and(criteriaBuilder.equal(root.get("level"), level),
                                           criteriaBuilder.equal(root.get("modelId"), modelId),
                                           criteriaBuilder.greaterThanOrEqualTo(root.get("epochTimeMax"), epochTimeMin),
                                           criteriaBuilder.lessThanOrEqualTo(root.get("epochTimeMin"), epochTimeMax)));

        return session.createQuery(criteriaQuery).getSingleResult();
    }

    private Long executeCriteriaQuery(Session session,
                                      long modelId,
                                      ZonedDateTime start,
                                      ZonedDateTime end) {
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<ExecEntity> root = criteriaQuery.from(ExecEntity.class);

        criteriaQuery.select(criteriaBuilder.count(root))
                .where(criteriaBuilder.and(criteriaBuilder.equal(root.get("modelId"), modelId),
                                           criteriaBuilder.greaterThanOrEqualTo(root.get("timeCalculated"), start),
                                           criteriaBuilder.lessThanOrEqualTo(root.get("timeCalculated"), end)));

        return session.createQuery(criteriaQuery).getSingleResult();
    }
}
